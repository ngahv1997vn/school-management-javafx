package home.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ClassModel {

    private SimpleIntegerProperty classId;
    private SimpleStringProperty className;

    public ClassModel() {
    }

    public int getClassId() {
        return classId.get();
    }

    public SimpleIntegerProperty classIdProperty() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId.set(classId);
    }

    public String getClassName() {
        return className.get();
    }

    public SimpleStringProperty classNameProperty() {
        return className;
    }

    public void setClassName(String className) {
        this.className.set(className);
    }
}
