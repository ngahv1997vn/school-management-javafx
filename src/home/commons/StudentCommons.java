package home.commons;

import home.DAO.StudentDAOImpl;
import home.model.StudentsModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class StudentCommons {
    private StudentDAOImpl DAO = new StudentDAOImpl();

    private ObservableList<StudentsModel> studentsModels = FXCollections.observableArrayList(
            DAO.getStudentModel()
    );
    public void loadStudents(TableView<StudentsModel> tbData,
                             TableColumn<StudentsModel, Integer> studentId,
                             TableColumn<StudentsModel, String> firstName,
                             TableColumn<StudentsModel, String> lastName,
                             TableColumn<StudentsModel, Integer> idClass
                             )
    {
        studentId.setCellValueFactory(new PropertyValueFactory<>("StudentId"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("FirstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("LastName"));
        idClass.setCellValueFactory(new PropertyValueFactory<>("IdClass"));
        tbData.setItems(studentsModels);
    }


}
