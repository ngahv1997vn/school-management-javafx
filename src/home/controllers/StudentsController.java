package home.controllers;

import home.commons.StudentCommons;
import home.model.StudentsModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentsController implements Initializable {

    @FXML
    private TableView<StudentsModel> tbData;
    @FXML
    public TableColumn<StudentsModel, Integer> studentId;

    @FXML
    public TableColumn<StudentsModel, String> firstName;

    @FXML
    public TableColumn<StudentsModel, String> lastName;

    @FXML
    public TableColumn<StudentsModel, Integer> idClass;

//    private StudentDAOImpl DAO = new StudentDAOImpl();

    private StudentCommons studentCommons = new StudentCommons();

    public StudentsController()
    {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadStudents();
    }

//    private ObservableList<StudentsModel> studentsModels = FXCollections.observableArrayList(
//            DAO.getStudentModel()
//    );

    private void loadStudents()
    {
        studentCommons.loadStudents(tbData,studentId,firstName,lastName,idClass);
    }


}
