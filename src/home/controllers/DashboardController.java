package home.controllers;

import home.commons.StudentCommons;
import home.model.StudentsModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class DashboardController implements Initializable {

    @FXML
    private TableView<StudentsModel> tbData;
    @FXML
    public TableColumn<StudentsModel, Integer> studentId;

    @FXML
    public TableColumn<StudentsModel, String> firstName;

    @FXML
    public TableColumn<StudentsModel, String> lastName;

    @FXML
    public TableColumn<StudentsModel, Integer> idClass;

    @FXML
    private PieChart pieChart;

//    private StudentDAOImpl DAO = new StudentDAOImpl();

    private StudentCommons studentCommons = new StudentCommons();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loadChart();
        loadStudents();
    }

    private void loadChart()
    {

        PieChart.Data slice1 = new PieChart.Data("Classes", 213);
        PieChart.Data slice2 = new PieChart.Data("Attendance"  , 67);
        PieChart.Data slice3 = new PieChart.Data("Teachers" , 36);

        pieChart.getData().add(slice1);
        pieChart.getData().add(slice2);
        pieChart.getData().add(slice3);

    }

//    private ObservableList<StudentsModel> studentsModels = FXCollections.observableArrayList(
//            DAO.getStudentModel()
//    );

    private void loadStudents()
    {
        studentCommons.loadStudents(tbData,studentId,firstName,lastName,idClass);
    }
}
