package home.DAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class ClassDAOImpl implements DAO {
    public static Connection getConnection(String dbURL, String userName,
                                           String password) {
        Connection conn = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection(dbURL, userName, password);
//            System.out.println("Connect Successful");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return conn;
    }


}
