package home.DAO;

import home.model.StudentsModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements DAO {
    public static Connection getConnection(String dbURL, String userName,
                                           String password) {
        Connection conn = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection(dbURL, userName, password);
//            System.out.println("Connect Successful");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return conn;
    }

    private StudentsModel createStudentModel(ResultSet rs) {
        StudentsModel st = new StudentsModel();
        try {
            st.setStudentId(rs.getInt("id"));
            st.setFirstName(rs.getString("first_name"));
            st.setLastName(rs.getString("last_name"));
            st.setIdClass(rs.getInt("id_class"));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return st;
    }

    public List<StudentsModel> getStudentModel() {
        String sql = "Select * from Student";
        List<StudentsModel> list = new ArrayList<>();
        try {
            Connection con = getConnection(DB_URL,USER_NAME,PASSWORD);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                StudentsModel p = createStudentModel(rs);
                list.add(p);
            }
            rs.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }


}
